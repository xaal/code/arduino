
Sample program to use an CCS811 as a CO2 meter in xAAL. 
Tested on a ESP32 board and CJMCU-811v1 breakout.

This is a native xAAL device, no aditionnal gateway or 
broker needed.

To configure edit the include/config.h (ssid,password,
xaal key, and xaal device UUID). The default key is "xaal". 

This device doesn't handle incomming request (is_alive,
get_description..) but simply send this informations
at a fixed period of 120 sec. This is enough for sensors.

IDE: PlatformIO, because it handles dependencies.

Notes:
- Depends on :
  - YACL for CBOR
  - NTPClient for the NTP (needed by ChachaPoly)
  - Crypto for ChachaPoly
  - Adafruit CCS811 Library

- If you want to use the Lolin32 Lite, SCL & SDA won't
  work out of the box.
  Edit Adafruit_CCS811::_i2c_init. Wire.begin(..)

  
